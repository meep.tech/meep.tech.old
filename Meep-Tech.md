# Meep-Tech; Intro
This is the root of all meep-tech projects. This file will explain the folder structure and where to put different projects and how to create git repos for new projects.

## Key
__>__: Folder, >>.. levels deep

__+__: Use of the projects in this folder requires the download of a base repo

__*__: This folder is the root directory for individual projects. Every child directory should be it's own submodule.

# Base Codebase Folders:
These folders must be created and managed seperate from git repositories:
## >apps
These are frontend applications of different kinds.
### >>react-js __*+__
This folder contains react data for building react frontend applications.
+ __+__:To work with applications in this repo, you will need to download the 'apps-react-js' base repo.
#### >>>src/apps
This directory contains the react applications at the project level
+ __*__:Each folder within this directory should be a submodule

## >php __+__
This is the folder containing the php backend
+ __+__:To work with apllications within these directories, you will need to download the 'php-base' base repo.
### >>API __*__
This folder contains RESTful api endpoints that can access the backend applications and return serialized data to the frontend.
+ __*__:Each folder within this directory should be a submodule
### >>Projects __*__
This folder contains the submodules for project backends. Things such as models, stored procedures, database tables, enums, and DAOs should be stored within these folders.
+ __*__:Each folder within this directory should be a submodule

# Repos and Dependencies

- __Meep-Tech__: The base repo for the entire project.
    - __>__: This should go in _/meep-tech_

- __base-php__: The base php repo, generates the _php/API_ folder for api applications and the _php/Projects_ folder for backend applications. This repo also sets up the vendor and composer settings for php.
    - __>__: This should go in _meep-tech/php_
    - __!__: Requires __Meep-Tech__

- __apps-react-js__: The base repo for react frontend apps. and sets up the react build and test enviroments. This also creates the _react-js/scr/apps_ folder for react-app applications.
    - __>__: This should go in _meep-tech/apps/react-js_
    - __!__: Requires __Meep-Tech__

- __react-js-shared__: This is shared modules and components for react apps.
    - __>__: This should go in _react-js/src/apps/shared_
    - __!__: Requires __apps-react-js__
    
- __php-API-shared__: This repo contains the shared and base API files.
    - __>__: This should go in _php/API/shared_
    - __!__: Requires __base-php__
    
- __php-Projects-shared__: This repo contains the shared and base model and dao files for the php backend
    - __>__: This should go in _php/Projects/shared_
    - __!__: Requires __base-php__